#bodyMerge

This python script analyzes multiple body tracking files as generated through the Microsoft Kinect SDK and ChronoSense in order to create a single combined body tracking file.

##Using bodyMerge

This script only required Python 2.7 be installed in order to run.

To run the application enter:
python bodyMerge.py

Once open, a dialogue will open asking you to select the directory where you have the body tracking files you wish to analyze and combine.

The output file will match the input files but end with _bodyTrackingCombined.csv

##Current Parameters

The default settings for filtering out noise in the body tracking files for bodyMerge are as follows:

- Files with less than one minute of data are excluded
- Every two seconds body distance is assessed and if closer than 1 meter or farther than 2.8 meters that segment of data is ignored (assumed 30 Hz data sample rate)
