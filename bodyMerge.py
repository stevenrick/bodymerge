'''
Created on May 5, 2014

@author: rick
'''

import os
import re
import fnmatch
import numpy as np
import Tkinter, tkFileDialog


root = Tkinter.Tk()
root.withdraw()


def hms_to_seconds(tIn):
    
    tCells = tIn.split(':')
    if len(tCells) == 3:
        #has h:m:s
        return int(tCells[0]) * 3600 + int(tCells[1]) * 60 + float(tCells[2])
    else:
        if len(tCells) == 2:
            #has m:s
            return int(tCells[0]) * 60 + float(tCells[1])
        else:
            #has s
            return float(tCells[0])


def cleanCSVGaps(infile,tempname):
    
    #print 'No gaps here'
    tempFileName = tempname
    #print tempFileName
    
    csvFile = [line.split(',') for line in open(infile)]
    
    with open(tempFileName,'wb') as output:
        for row in csvFile:
            if len(row) > 2:
                output.write( ','.join(row) )
    output.close()


def windowedAnalysis(currentFile, writeDict, in_file):
    csvRows = [line.split(',') for line in open(in_file)]
    rawdata = np.genfromtxt(in_file,dtype='float',delimiter = ',',skiprows=1, skip_header=0, skip_footer=0, usecols=9,usemask=True, invalid_raise=False) 
    #print data
    windowCounter = 0
    globalCounter = 0
    sample = 0
    #windowStart = 0
    isWindowStart = True
    skipHeader = True
    #windowEnd = 0
    windowSize = 60 #30hz sample rate so window size in seconds = windowSize/30
    tempdata = []
    writeHolder = []
    lineHolder = []
    dataToWrite = []
    for el in rawdata:
        #print el
        #print windowCounter
        if windowCounter < windowSize:
            if isWindowStart:
                #windowStart = globalCounter
                isWindowStart = False
            tempdata.append(el)
            windowCounter = windowCounter + 1
            #windowEnd = globalCounter
            globalCounter = globalCounter + 1
        else:
            if len(tempdata)>2:
                #print 'temp:'
                #print tempdata
                sample = sample + 1
                #print "Sample "+str(sample)
                #print "Global sample "+str(globalCounter)
                #print "Window Start: "+str(windowStart)
                #print "Window End: "+str(windowEnd)
                #sampleStd = np.std(tempdata)
                #sampleRange = (np.max(tempdata) - np.min(tempdata))
                sampleAvg = np.average(tempdata)
                
                #analyze windowed sample of data
                #print "sd: "+str(sampleStd)
                #print "range: "+str(sampleRange)
                #print "avg: "+str(sampleAvg)
                #print ""
                if sampleAvg > 1 and sampleAvg < 2.8:
                    #print "wrote sample at: "+str(globalCounter)
                    writeHolder.append(sample)
                else:
                    #print "ignored sample at: "+str(globalCounter)
                    continue
                
                #clear sampled data after analysis
                tempdata = []
                isWindowStart = True
                windowCounter = 0
            else:
                #print 'empty tempdata'
                continue
            
    for item in writeHolder:
        startFrame = ((item-1)*windowSize)
        endFrame = ((item*windowSize)-1)
        n = startFrame
        while n <= endFrame:
            lineHolder.append(n)
            n = n + 1
            
    for number in lineHolder:
        if skipHeader:
            header = csvRows[number]
            header = header
            #print 'header:'
            #print header
            skipHeader = False
        else:
            dataToWrite.append(csvRows[number][0])
    #print 'write:'
    #print writeHolder
    #print 'line:'
    #print lineHolder
    writeDict.update({currentFile:dataToWrite})
    #print writeDict
    return writeDict


def fileProfiler(inputFile):
    #print inputFile
    with open(inputFile, 'rb') as bodyInput:
        
        kinectBodyCSV = [line.split(',') for line in bodyInput]
        
        averageDepth = 0.0
        distanceTotal = 0.0
        counter = 0
        fileLength = 0.0
        firstSample = True
        skipHeader = True
        # go through each line of the file skipping header (first row)
        
        for row in kinectBodyCSV:
            if skipHeader:
                skipHeader = False
                continue
            else:
                if firstSample:
                    startTime = hms_to_seconds(row[0])
                    firstSample = False
                else:
                    endTime = hms_to_seconds(row[0])
                if len(row)>=9:
                    if row[9] != '':
                        distanceTotal = distanceTotal + float(row[9])
                        counter = counter + 1
                else:
                    #print 'depth empty'
                    continue    
        if counter != 0:
            averageDepth = distanceTotal/counter
            fileLength = endTime - startTime
            
    bodyInput.close()
    return averageDepth, fileLength


def writeToFile(writeMeDict, fileList, savePath):
    writeHeader = True
    for key in sorted((writeMeDict), key=numericalSort):
        tempFile = fileList.get(key)
        #print key
        #print tempFile
        with open(tempFile, 'rb') as csvRead, open(savePath, 'a') as csvWrite:
            for row in csvRead:
                if writeHeader:
                    csvWrite.write((row)[:-1])
                    writeHeader = False
                else:
                    if any(row[0] in s for s in writeMeDict.get(key)):
                        #print 'match'
                        csvWrite.write((row)[:-1])
        csvWrite.close()
        csvRead.close()


def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


def main(directory):
    runOnce = True
    writeDict = {}
    fileList = {}
    
    for filename in sorted((os.listdir(directory)), key=numericalSort):
        if fnmatch.fnmatch(filename, '*bodyTracking.csv'):
            filepath = os.path.join(directory, filename)
            currentFile = (filename.split('_'))[-2]
            print currentFile
            fileList.update({currentFile:filepath})
            
            if runOnce:
                saveNameTemp = (filename.split('_'))[:-2]
                saveName = '_'.join(saveNameTemp)
                bodySavepath = os.path.join(directory, saveName+"_bodyTrackingCombined.csv")
                if os.path.isfile(bodySavepath):
                    os.remove(bodySavepath)
                runOnce = False
                
            temppath = os.path.splitext(filepath)[0]+"_temp.csv"
            
            inputFile = r""+filepath+""
            tempFile = r""+temppath+""
            
            #print inputFile
            depthAverage, fileLength = fileProfiler(inputFile)
            depthAverage = depthAverage
            #print depthAverage
            #print fileLength
            if fileLength > 60:
                cleanCSVGaps(inputFile, tempFile)
                writeDict.update(windowedAnalysis(currentFile, writeDict, tempFile))
                os.remove(tempFile)
            else:
                #skipping short bodytracking files
                print "File skipped"
                continue
        else:
            #skipping non-bodytracking files
            continue
    
    #print writeDict
    #print fileList
    writeToFile(writeDict, fileList, bodySavepath)
    
    
    print 'Merge Complete'


print 'Starting Merge'
numbers = re.compile(r'(\d+)')
'''
if running script by itself, uncomment below
'''

directory = tkFileDialog.askdirectory()
main(directory)